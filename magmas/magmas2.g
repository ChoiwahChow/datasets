AllMagmas2_CayleyTable := [
[[ 1,1 ],[ 1,1 ]],
[[ 1,1 ],[ 1,2 ]],
[[ 1,1 ],[ 2,1 ]],
[[ 1,1 ],[ 2,2 ]],
[[ 1,2 ],[ 1,1 ]],
[[ 1,2 ],[ 1,2 ]],
[[ 1,2 ],[ 2,1 ]],
[[ 2,1 ],[ 1,1 ]],
[[ 2,1 ],[ 2,1 ]],
[[ 2,2 ],[ 1,1 ]],
];;
AllMagmas2 := List(AllMagmas2_CayleyTable, G->MagmaByMultiplicationTable(G));;
AllMagmas2_CayleyTable = [];;
